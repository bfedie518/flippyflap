extends Node


export var pipe_scene: PackedScene
export var pipe_min_height: float = 144.0
export var pipe_max_height: float = 422.0
export var pipe_spawn_x: float = 380.0
export var max_diff: float = 150.0

var _score: int = 0
var _prev_pipe_height: float

onready var game_over_screen := get_node("UI/GameOver") as Control


func _ready() -> void:
	randomize()
	game_over_screen.hide()


func _on_PipeTimer_timeout() -> void:
	var new_pipe_height: float
	var diff: float
	while new_pipe_height == 0.0 or diff > max_diff:
		new_pipe_height = rand_range(pipe_min_height, pipe_max_height)
		diff = abs(new_pipe_height - _prev_pipe_height)

	var new_pipe := pipe_scene.instance() as Pipe
	new_pipe.position.x = pipe_spawn_x
	new_pipe.position.y = new_pipe_height
	_prev_pipe_height = new_pipe_height
	add_child(new_pipe)


func _on_Button_pressed() -> void:
# warning-ignore:return_value_discarded
	get_tree().reload_current_scene()


func _on_Player_area_entered(area: Area2D) -> void:
	if area.is_in_group("obstacles"):
		($PipeTimer as Timer).stop()
	$Player.set_process(false)
	for member in get_tree().get_nodes_in_group("pipes"):
		member.set_process(false)
	game_over_screen.show()


func _on_ScoreArea_area_exited(area: Area2D) -> void:
	if area.is_in_group("pipes"):
		_score += 1
		($UI/ScoreLabel as Label).text = str(_score)
