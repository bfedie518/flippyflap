class_name Player
extends Area2D


export var acceleration: float = 10.0
export var jump_impulse: float = 5.0
var _velocity: float = 0.0


func _process(delta: float) -> void:
	_velocity += acceleration * delta

	if Input.is_action_just_pressed("jump"):
		_velocity = -jump_impulse

	position.y += _velocity

	if position.y < 0:
		position.y = 0
